package com.screenmeet.capturesdk.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by yuliiaS on 7/4/16.
 */
public class DrawView extends View {

    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private Paint drawPaint, canvasPaint;
    //initial color
    private int paintColor = 0xFFFFF500;

    private int strokeWidth = 14;
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpDrawing();
    }


    private void setUpDrawing() {

        drawPath = new Path();
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(strokeWidth);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    public void setPaintColor(int paintColor) {

        invalidate();
        this.paintColor = paintColor;
        drawPaint.setColor(paintColor);
    }

    public void setStrokeWidth(int strokeWidth) {
        invalidate();
        this.strokeWidth = strokeWidth;
        drawPaint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (canvasBitmap == null) {
            canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        } else if (h != canvasBitmap.getHeight()) {
            /*bitmap width is bigger because of image you get from ImageReader has some extra pixels in there on each line*/
            canvasBitmap = Bitmap.createScaledBitmap(canvasBitmap, canvasBitmap.getWidth() * h / canvasBitmap.getHeight(), h, false);
        }
        drawCanvas = new Canvas(canvasBitmap);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        //  super.onDraw(canvas);

        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }

    public void setCanvasBitmap(Bitmap canvasBitmap) {
        this.canvasBitmap = canvasBitmap;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPoint(touchX, touchY, drawPaint);
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    public void cleanView(Bitmap canvasBitmap) {
        drawPath.reset();

        drawPath = new Path();
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(strokeWidth);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);
        this.canvasBitmap = canvasBitmap;
        if (canvasBitmap.getHeight() != getHeight()) {
            onSizeChanged(getWidth(), getHeight(), getWidth(), getHeight());
        } else {
            drawCanvas = new Canvas(canvasBitmap);
        }
        invalidate();
    }

    public void recycleBitmap(){
        canvasBitmap.recycle();
        canvasBitmap=null;
    }
}
