package com.screenmeet.capturesdk.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.screenmeet.api.ScreenMeetAPI;
import com.screenmeet.api.callback.AuthenticationCallback;
import com.screenmeet.capturesdk.R;
import com.screenmeet.sdk.ScreenMeet;


public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getName();
    EditText emailEdt, nameEdt, passwordEdt;
    Button signUpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        emailEdt = (EditText) findViewById(R.id.emailEdt);
        nameEdt = (EditText) findViewById(R.id.nameEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);

        signUpBtn = (Button) findViewById(R.id.signUpBtn);

        setOnClickListener();
    }

    private void setOnClickListener() {
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.initInstance(ScreenMeet.EnvironmentType.SANDBOX, null);
                ScreenMeet.getInstance().createUser(emailEdt.getText().toString(),
                        nameEdt.getText().toString(),
                        passwordEdt.getText().toString(),
                        new AuthenticationCallback() {
                            @Override
                            public void failure(String error, int statusCode) {
                                Log.e(TAG, "authenticate error " + error);
                            }

                            @Override
                            public void success() {
                                Log.i(TAG, "authenticate success ");

                                Intent signInIntent = new Intent(SignUpActivity.this, FirstCaptureActivity.class);
                                startActivity(signInIntent);
                                finish();
                            }
                        });
            }
        });
    }
}
