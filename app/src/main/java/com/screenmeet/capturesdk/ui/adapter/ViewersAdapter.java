package com.screenmeet.capturesdk.ui.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.screenmeet.api.socket.attendees.Viewer;
import com.screenmeet.capturesdk.R;
import com.screenmeet.sdk.ScreenMeet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuliia on 8/12/16.
 */
public class ViewersAdapter extends ArrayAdapter<Viewer> {
    private Context context;
    List<Viewer> items;

    public ViewersAdapter(Context context, List<Viewer> items) {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
        this.items = items;
    }

    /**
     * Holder for the list items. Will be modified outside adapter, so keep public.
     */
    public class ViewHolder {
        public TextView titleText;
        public ProgressBar spinner;
        public TextView kickButton;
        public ImageView attendeeDeviceImg;
        public ImageView leftImg;

        /*This one cant be modified*/
        private int index;
    }

    @Override
    public void remove(Viewer object) {
        this.items.remove(object);
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Viewer item = (Viewer) getItem(position);
        View view = null;

        // This block exists to inflate the settings list item conditionally based on whether
        // we want to support a grid or list view.
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // if (convertView == null) {
        view = mInflater.inflate(R.layout.attendees_item, null);

        holder = new ViewHolder();
        holder.titleText = (TextView) view.findViewById(R.id.titleTextView);

        //handle the "kick" button
        holder.kickButton = (TextView) view.findViewById(R.id.kickButton);
        holder.index = position;

        //handle the spinner
        holder.spinner = (ProgressBar) view.findViewById(R.id.proressBar);
        holder.spinner.getIndeterminateDrawable().setColorFilter(0xffcccccc, android.graphics.PorterDuff.Mode.MULTIPLY);

            /*Here goes the right view elements like device, browser, screen resolution info*/
        //holder.attendeeScreenResolutionImg = (ImageView)view.findViewById(R.id.attendeeScreenResolutionImg);
        //holder.attendeeScreenResolutionTxt = (TextView)view.findViewById(R.id.attendeeScreenResolutionTxt);
        holder.attendeeDeviceImg = (ImageView) view.findViewById(R.id.attendeeDeviceImg);

        //holder.attendeeDeviceTxt = (TextView)view.findViewById(R.id.attendeeDeviceTxt);
        //holder.attendeeBrowserImg = (ImageView)view.findViewById(R.id.attendeeBrowserImg);
        //holder.attendeeBrowserTxt = (TextView)view.findViewById(R.id.attendeeBrowserTxt);

        holder.leftImg = (ImageView) view.findViewById(R.id.leftImg);
        //holder.delayTxt = (TextView)view.findViewById(R.id.delayTxt);

        restoreState(holder);
        setupKickButton(holder);

            /*Here goes the setup of right view elements like device, browser, screen resolution info*/
        //setupAdditionalInfo(holder);

        view.setTag(holder);

        holder.titleText.setText(item.getName());
        return view;
    }

    /*Remove the spinner and place "close / kick attendee" button*/
    private void restoreState(ViewHolder holder) {
        holder.kickButton.setVisibility(View.VISIBLE);
        holder.spinner.setVisibility(View.GONE);
    }

    /*set the kick button click handler*/
    private void setupKickButton(final ViewHolder holder) {
        holder.kickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Send analytics*/

                if (ScreenMeet.getInstance().getViewerCount() > holder.index) {
                    Viewer attendee = items.get(holder.index);
                    showKickConfirmationAlert(attendee, holder);
                }
            }
        });
    }

    private void showKickConfirmationAlert(final Viewer attendee, final ViewHolder holder) {
        new AlertDialog.Builder(context)
                .setTitle("Do you want to kick user " + attendee.getName() + " ?")
                .setPositiveButton("Kick out", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Display the spinner
                        holder.kickButton.setVisibility(View.GONE);
                        holder.spinner.setVisibility(View.VISIBLE);

                        ScreenMeet.getInstance().kickViewer(attendee.getId());
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public void setItems(ArrayList<Viewer> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Viewer getItem(int position) {
        return items.get(position);
    }
}
