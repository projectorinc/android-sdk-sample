package com.screenmeet.capturesdk.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.screenmeet.api.StreamState;
import com.screenmeet.api.StreamStateChangeCallback;
import com.screenmeet.api.StreamStateChangeReason;
import com.screenmeet.api.socket.StreamCallback;
import com.screenmeet.api.socket.ViewerCallback;
import com.screenmeet.api.socket.attendees.Viewer;
import com.screenmeet.capturesdk.R;
import com.screenmeet.capturesdk.ui.activity.parent.ParentCaptureActivity;
import com.screenmeet.capturesdk.ui.adapter.ViewersAdapter;
import com.screenmeet.sdk.ScreenMeet;

import org.json.JSONObject;

import java.util.ArrayList;

public class FirstCaptureActivity extends ParentCaptureActivity {

    private static final String TAG = FirstCaptureActivity.class.getName();
    TextView linkTV;

    Button startBtn, stopBtn, pauseBtn, resumeBtn;

    ListView viewersLV;

    ViewersAdapter adapter;

    View stopLL;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_capture);

        linkTV = (TextView) findViewById(R.id.roomLink);

        startBtn = (Button) findViewById(R.id.startBtn);
        stopBtn = (Button) findViewById(R.id.stopBtn);
        pauseBtn = (Button) findViewById(R.id.pauseBtn);

        resumeBtn = (Button) findViewById(R.id.resumeBtn);

        stopLL = findViewById(R.id.stopLL);

        adapter = new ViewersAdapter(this, ScreenMeet.getInstance().getViewers());
        viewersLV = (ListView) findViewById(R.id.bottomAttendeesList);
        viewersLV.setAdapter(adapter);

        linkTV.setText(ScreenMeet.getInstance().getRoomUrl() + "");

        setOnClickListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
         ScreenMeet.getInstance().setStreamSource(FirstCaptureActivity.this,null);
    }

    private void setOnClickListener() {

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBtn.setEnabled(false);
                progress = ProgressDialog.show(FirstCaptureActivity.this, "Starting meeting",
                        "Wait please", true);
                ScreenMeet.getInstance().startStream(new StreamCallback() {
                    @Override
                    public void onSuccess(JSONObject json) {

                        progress.dismiss();
                        stopLL.setVisibility(View.VISIBLE);
                        startBtn.setEnabled(true);
                        startBtn.setVisibility(View.GONE);

                        Log.e(TAG, "onSuccess start");
                        Toast.makeText(FirstCaptureActivity.this, "Meeting started", Toast.LENGTH_SHORT).show();

                        ScreenMeet.getInstance().setViewerJoinedHandler(new ViewerCallback() {
                            @Override
                            public void onResult(Viewer viewer) {
                                adapter = new ViewersAdapter(FirstCaptureActivity.this, ScreenMeet.getInstance().getViewers());
                                viewersLV.setAdapter(adapter);
                            }
                        });

                        ScreenMeet.getInstance().setViewerLeftHandler(new ViewerCallback() {
                            @Override
                            public void onResult(Viewer viewer) {
                                adapter = new ViewersAdapter(FirstCaptureActivity.this, ScreenMeet.getInstance().getViewers());
                                viewersLV.setAdapter(adapter);
                            }
                        });


                    }

                    @Override
                    public void onFailure(String error) {
                        startBtn.setEnabled(true);
                        Log.e(TAG, "onFailure start");
                        Toast.makeText(FirstCaptureActivity.this, "cannot start meeting, error: " + error, Toast.LENGTH_SHORT).show();
                        progress.dismiss();
                    }

                });
            }
        });

        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().pauseStream();

            }
        });

        resumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().resumeStream();
                //Toast.makeText(FirstCaptureActivity.this, "resume meeting", Toast.LENGTH_SHORT).show();

            }
        });


        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().stopStream();
            }
        });

        ScreenMeet.getInstance().setStreamStateChangedHandler(new StreamStateChangeCallback() {
            @Override
            public void onChange(StreamState streamState, StreamStateChangeReason streamStateChangeReason) {
                if (streamState == StreamState.ACTIVE) {

                    resumeBtn.setVisibility(View.GONE);
                    pauseBtn.setVisibility(View.VISIBLE);
                }

                if (streamState == StreamState.STOPPED) {
                    stopLL.setVisibility(View.GONE);

                    startBtn.setVisibility(View.VISIBLE);
                    resumeBtn.setVisibility(View.GONE);
                    pauseBtn.setVisibility(View.VISIBLE);

                    adapter = new ViewersAdapter(FirstCaptureActivity.this, new ArrayList<Viewer>());
                    viewersLV.setAdapter(adapter);

                    Toast.makeText(FirstCaptureActivity.this, "meeting stopped", Toast.LENGTH_SHORT).show();
                }

                if (streamState == StreamState.PAUSED) {
                    Toast.makeText(FirstCaptureActivity.this, "meeting paused", Toast.LENGTH_SHORT).show();
                    pauseBtn.setVisibility(View.GONE);
                    resumeBtn.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.first_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.draw:
                Intent intentDraw = new Intent(FirstCaptureActivity.this, DrawActivity.class);
                startActivity(intentDraw);
                return true;
            case R.id.share:
                ScreenMeet.getInstance().showInviteMeetingLinkDialog(FirstCaptureActivity.this,"Share your room link: ");
                return true;
            case R.id.settings:
                Intent intent = new Intent(FirstCaptureActivity.this, ConfigActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }
}
