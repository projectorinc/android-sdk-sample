package com.screenmeet.capturesdk.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.screenmeet.capturesdk.R;
import com.screenmeet.capturesdk.ui.widget.DrawView;
import com.screenmeet.sdk.ScreenMeet;

public class DrawActivity extends AppCompatActivity {

    DrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);

        drawView=(DrawView)findViewById(R.id.drawView) ;

        ScreenMeet.getInstance().setStreamSource(DrawActivity.this,null);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        drawView.recycleBitmap();
    }
}
