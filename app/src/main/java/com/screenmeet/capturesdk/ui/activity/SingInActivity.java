package com.screenmeet.capturesdk.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.screenmeet.api.callback.AuthenticationCallback;
import com.screenmeet.capturesdk.R;
import com.screenmeet.sdk.ScreenMeet;

public class SingInActivity extends AppCompatActivity {

    private static final String TAG = SingInActivity.class.getName();
    Button signIn;
    EditText emailEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        emailEditText = (EditText) findViewById(R.id.emailEdt);
        passwordEditText = (EditText) findViewById(R.id.passwordEdt);

        signIn = (Button) findViewById(R.id.signInBtn);

        setOnClickListener();
    }

    private void setOnClickListener() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.initInstance(ScreenMeet.EnvironmentType.SANDBOX, null);
                ScreenMeet.getInstance().authenticate(emailEditText.getText().toString(), passwordEditText.getText().toString(), new AuthenticationCallback() {
                    @Override
                    public void failure(String error, int statusCode) {
                        Log.e(TAG, "authenticate error " + error);
                        Toast.makeText(SingInActivity.this, error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void success() {
                        Log.i(TAG, "authenticate success ");

                        Intent signInIntent = new Intent(SingInActivity.this, FirstCaptureActivity.class);
                        startActivity(signInIntent);
                        finish();
                    }
                });
            }
        });
    }
}
