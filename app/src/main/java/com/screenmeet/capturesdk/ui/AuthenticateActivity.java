package com.screenmeet.capturesdk.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.screenmeet.capturesdk.R;
import com.screenmeet.capturesdk.ui.activity.BearerActivity;
import com.screenmeet.capturesdk.ui.activity.SignUpActivity;
import com.screenmeet.capturesdk.ui.activity.SingInActivity;

public class AuthenticateActivity extends AppCompatActivity {

    Button sinIn, signUp, signBearerToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autentificate);

        sinIn = (Button) findViewById(R.id.signInBtn);
        signUp = (Button) findViewById(R.id.signUp);
        signBearerToken = (Button) findViewById(R.id.bearSignIn);

        setOnClickListeners();
    }

    private void setOnClickListeners() {
        sinIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = new Intent(AuthenticateActivity.this, SingInActivity.class);
                startActivity(signInIntent);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bearerIntent = new Intent(AuthenticateActivity.this, SignUpActivity.class);
                startActivity(bearerIntent);
            }
        });

        signBearerToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bearerIntent = new Intent(AuthenticateActivity.this, BearerActivity.class);
                startActivity(bearerIntent);
            }
        });

    }
}
