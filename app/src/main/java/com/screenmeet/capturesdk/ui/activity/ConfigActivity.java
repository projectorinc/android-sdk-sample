package com.screenmeet.capturesdk.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.screenmeet.api.callback.SetRoomNameCallback;
import com.screenmeet.capturesdk.R;
import com.screenmeet.capturesdk.ui.AuthenticateActivity;
import com.screenmeet.sdk.ScreenMeet;

public class ConfigActivity extends AppCompatActivity {

    Button signOutBtn, changeRoomNameBtn, changeMeetingConfigBtn;

    TextView resetPasswordTV, roomNameET, meetingPasswordET, imageQualityET;

    CheckBox askForNameChBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        signOutBtn = (Button) findViewById(R.id.signOutBtn);
        changeRoomNameBtn = (Button) findViewById(R.id.changeRoomNameBtn);
        changeMeetingConfigBtn = (Button) findViewById(R.id.changeMeetingConfigBtn);

        resetPasswordTV = (TextView) findViewById(R.id.resetPasswordTV);
        roomNameET = (EditText) findViewById(R.id.roomNameET);
        meetingPasswordET = (EditText) findViewById(R.id.roomPasswordET);
        imageQualityET=(EditText)findViewById(R.id.imageQualityET);

        askForNameChBox = (CheckBox) findViewById(R.id.askNameChBox);

        setText();

        setOnClickEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        ScreenMeet.getInstance().setStreamSource(ConfigActivity.this, null);
    }

    private void setText() {
        resetPasswordTV.setText(ScreenMeet.getInstance().getResetPasswordURL(ScreenMeet.getInstance().getUserEmail()));
        roomNameET.setText(ScreenMeet.getInstance().getRoomName());

        askForNameChBox.setChecked(ScreenMeet.getInstance().getMeetingConfig().isAskViewerForName());
        meetingPasswordET.setText(ScreenMeet.getInstance().getMeetingConfig().getRoomPassword()+"");
        imageQualityET.setText(ScreenMeet.getInstance().getMeetingConfig().getImageQuality()+"");
    }

    private void setOnClickEvents() {
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().logoutUser();

                Toast.makeText(ConfigActivity.this, "sign Out", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(ConfigActivity.this, AuthenticateActivity.class);
                startActivity(intent);
                finish();
            }
        });

        changeRoomNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().setRoomName(roomNameET.getText().toString(), new SetRoomNameCallback() {
                    @Override
                    public void success() {
                        Toast.makeText(ConfigActivity.this, "Room Name changed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(String error, int statusCode) {
                        Toast.makeText(ConfigActivity.this, "Room Name not changed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        changeMeetingConfigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.getInstance().setMeetingConfig(meetingPasswordET.getText().toString(), askForNameChBox.isChecked(), Integer.parseInt(imageQualityET.getText().toString()));
                Toast.makeText(ConfigActivity.this, "Meeting Config Changed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
