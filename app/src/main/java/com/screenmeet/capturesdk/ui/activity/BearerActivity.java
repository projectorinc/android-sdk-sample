package com.screenmeet.capturesdk.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.screenmeet.api.ScreenMeetAPI;
import com.screenmeet.api.callback.AuthenticationCallback;
import com.screenmeet.capturesdk.R;
import com.screenmeet.sdk.ScreenMeet;


public class BearerActivity extends AppCompatActivity {

    private static final String TAG = BearerActivity.class.getName();
    EditText bearerText;
    Button signInBtn, revertBearerBtn;

    String bearer = "3049:$2a$10$ulOYXT9WkMFGCJcAMZIw7u2jqNOHxi7K4iCX6QGzlqY6LwZmlggQ6";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bearer);

        bearerText = (EditText) findViewById(R.id.bearer);

        signInBtn = (Button) findViewById(R.id.signInBtn);
        revertBearerBtn = (Button) findViewById(R.id.revertBearer);

        setOnCLickListeners();
    }

    private void setOnCLickListeners() {
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenMeet.initInstance(ScreenMeet.EnvironmentType.SANDBOX, null);
                ScreenMeet.getInstance().authenticate(bearerText.getText().toString(), new AuthenticationCallback() {
                    @Override
                    public void failure(String error, int statusCode) {
                        Log.e(TAG, " bearer authenticate error " + error);
                    }

                    @Override
                    public void success() {
                        Log.i(TAG, "authenticate success ");

                        Intent signInIntent = new Intent(BearerActivity.this, FirstCaptureActivity.class);
                        startActivity(signInIntent);
                        finish();
                    }
                });
            }
        });

        revertBearerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bearerText.setText(bearer);
            }
        });
    }
}
